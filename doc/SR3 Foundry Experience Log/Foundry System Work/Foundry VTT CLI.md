The `npm` package manager can deliver the `fvtt` command line utility. If you have a need to dump one of the `data/` compendiums in a game system, you can install it, but you'll need to do some set up.
## Prerequisites

You'll need to install Node.js for your system. I'm running Windows, so these instructions fit that. Node.js installs the `npm` command, which is the Node.js package manager.
## Running External Scripts

By default, PowerShell doesn't allow you to run external scripts. You can switch to `cmd` if you don't want that restriction, but PowerShell is more robust. You can change this restriction in two ways:

If you want to do it in the  VS Code terminal, you'll need to create the `shadowrun3dev/.vscode/settings.json` file and populate it with the following content and then restart VS Code:

```json
{
	"terminal.integrated.profiles.windows": {
        "PowerShell": {
          "source": "PowerShell",
          "icon": "terminal-powershell",
          "args": ["-ExecutionPolicy", "Bypass"]
        }
    },
    "terminal.integrated.defaultProfile.windows": "PowerShell",
}
```

If you want to do it straight in PowerShell *everywhere*, you can open PowerShell as Administrator from the Start menu and then input the following command:

```powershell
Set-ExecutionPolicy remotesigned
```

Once you've done at least one of these, you can proceed.
## Installing Foundry VTT CLI

From command line, run

```powershell
npm install -g @foundryvtt/foundryvtt-cli
```

This should install the Foundry CLI program as `fvtt`. More information on the full package can be found at [their GitHub page](https://github.com/foundryvtt/foundryvtt-cli).

## Extracting the shadowrun3dev Data to JSON

**Big Note:** The `fvtt` program assumes you're going to perform all of your commands in the `AppData/Local/FoundryVTT` directory, so you should perform all of your commands from there. Failure to do so will cause malformed paths in the `fvtt` program and it will never find your files. 

Whenever you see `YOUR_WORLD_NAME`, you need to enter the actual world name that stores your data. This should be obvious, but in practice things rarely are.

Start by going to `AppData/Local/FoundryVTT/Data/worlds/YOUR_WORLD_NAME/` and make a copy of the `data/` directory with the name `packs/`.

Next, go to `AppData/Local/FoundryVTT` and open a PowerShell, then enter the following command:

```powershell
fvtt package workon "YOUR_WORLD_NAME" --type "World"
```

The Foundry CLI needs to be "set" to a specific module, system, or world. In practice, it seems to treat everything as a package, although it does know how to differentiate between each type of path.

It should tell you it's now working from your world directory. Next, you can look through your `packs/` directory and see which type of compendium you want to dump. For example, you could dump all of the actor information. To do that, you can enter the command:

```powershell
fvtt package unpack "actors"
```

The `fvtt` command will tell you it's trying to dump your `packs/actors` into `packs/actors/_source`. If it was successful (it found the `ldb` file *AND* that file had content in it), it will also say that it copied each entry in the compendium into its own JSON file in the relevant `_source` directory, where they can be freely viewed.