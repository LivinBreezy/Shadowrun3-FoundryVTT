This appears to be a system that has to exist because of the way webdev functions.

My initial assumption was that the Actor is the main repository of information, but it looks like that isn't really true. The true representation of a character is actually the ***sheet*** its attached to and that sheet's ***context***. I'm going to try to break it down as I understand it and I think it's set up this way because of how CSS/web works as a UI.

There are three things kind of stuck together: `Actor`, `ActorSheet`, and a kind of secret (shh, don't tell) data structure called a `context` object (I told you not to tell). 

There's a pipeline to get from an `Actor`'s and `ActorSheet`'s definition to being able to see the stats on a screen in a web element. From what I can tell, it's approximately this:

![[actor_lifecycle.png]]

Which means you prep with the following steps (for SR3):

1. Extend the `Actor` class to make `SR3Actor`. 
2. In `SR3Actor` you supposedly need to extend some methods, but either they do nothing or are doing something and my console log is not reporting.
3. Extend the `ActorSheet` class to make `SR3ActorSheet`.
4. Register your `SR3ActorSheet` so it replaces the default actor sheet.
5. The `getData()` method in `SR3ActorSheet` *needs* to be overridden so you can tell it how to re-calculate everything on the sheet. 

When you interact with your character sheet (which requires CSS/HTML files that I won't talk about here) by changing a value, it appears to run an `update` method call, which calls `getData()` on the `ActorSheet`. Because `SR3ActorSheet.getData()` overrides `ActorSheet.getData()`, we need to call `super.getData()` to get the `context` from the web UI.

The context object from the web UI is kind of strange, but my guess is that some of it exists for backwards compatibility of some sort. It stores the `Actor` object (which is actually an `SR3Actor` because of upcasting) represented by the `SR3ActorSheet`, but also stores some of the Actor's data in the `context.data` field. Not everything. Just some of it.

It also stores a number of things which are inside the `Actor` object, seemingly for convenience. The `Actor`'s `system`, `items`, and `effects` fields are stored here. This is a collection of data that the web UI uses to handle sheet rendering, ***but you don't get to access it without a getData call***.

I tested this myself in SR3 and it all seemed to line up. The Boilerplate `getData` method seemed to never be called, so there must be something else going on that I don't understand.

### Confusion

This whole process was elusive because it's hard to get ahold of the `context` object in the console from other game systems, like Boilerplate, to confirm that things are occurring this way.

It also seems like a lot of systems use this setup to manage their inventory systems for handling Item objects in the game.

For example, Boilerplate's `getData` has a function specifically for adding arrays of each item type currently held by the player to the `context`, which seems counter-intuitive because you'd think those inventories would be an aspect of the `Actor`. All I can conclude is that system devs simply look at the more filtered item arrays as a temporary representation method for the purposes of populating the Web UI only, since the `items` collection already exists on the `Actor` itself.

I guess I'll set up my code to do it that way simply for the purposes of convention. It just seems like a weird idea to start jamming new fields/arrays into existing objects, especially when it hides those fields from the `Actor`, and the user in the console.

I don't have an easy way of looking at the `SR3ActorSheet` in the console, so I actually don't know what its fields look like to know if it stores an `Actor` itself (and thus the `Actor` is basically just exclusively a data object) or if it references an `Actor` stored in the system somewhere else. I know it stores `Actors` in the `.db` files, but the lines get a little blurry between the sheet and the actor.

##### Update 1 (June 20, 2024)

With more investigation, it appears that the context being passed is literally contextual to the type of thing you're working on. The Foundry documentation notes the context object from `getData` is of one particular format which does not apply when working with the `ActorSheet`. As such, the context object needs to be viewed and noted in documentation in every possible sheet to make note of what it actually receives.

It appears that this context is saved between Foundry game sessions, which is to say that if I add a field to the context object it will still exist after Foundry is turned off and on again. The `.ldb` files are not as nice as they used to be when they were `.db` files and could be viewed in plain text, so it's hard to look inside them and see what they have stored now. I tried a few different viewers and none of them worked.

It appears Foundry has their own CLI for it via `npm`. I'll install it and see how it goes.

I dumped the `.ldb` files with Foundry's CLI and there was nothing to indicate it stored the `context.active_skills` field I made within `SR3ActorSheet`. I tried a fresh restart and tried a different browser to see if it was a cache thing in Foundry or Firefox, but it still shows up. I guess the next step would be to look through `foundry.js`, which is unappealing.

I took a quick look through the classes themselves in `foundry.js`, but there wasn't an obvious indication of how they're stored between sessions. It may be an `Application` thing, way up the class hierarchy. It does confirm that each class in the line builds their own context object on top of their parent class's context object, sometimes omitting fields.