This is where I'll include information on derived data, which are the properties of Actors and Items that are not provided by the template file and are instead calculated every time an Actor or Item sheet is opened/changed.

This information is not readily accessible. I've learned some of it from documentation and some of it from the Boilerplate tutorials. I find the lack of easily accessible documentation incredibly annoying.

## How Derived Values are Calculated

Reading through tutorials made it look like there was a set of `prepareData` methods called by the Actor class whenever the given Actor was registered, but this appears to actually not be the case as I can't get `console.log` to fire from inside them. 

Instead, the place where updates seem to happen most readily is during `ActorSheet.getData` from the ActorSheet class. This *appears* to be called (possibly during an `update` method call) when a sheet is first rendered (opened for the first time) or whenever a value inside the sheet has been changed. The `getData` method is provided the new context (AKA the entire new state of the Actor or Item) which you can then change at your leisure.

For example, if the character's Stun track increases from 3 (M) to 6 (S), then the total modifier from injuries should increase from 2 to 3. You can recalculate this modifier value during `ActorSheet.getData` to ensure your actor's information is up-to-date. You don't need to manually call any `update` methods. After you update the incoming context, you return it and it is updated in the game's database.

## Example Code for Injury Modifier

```javascript
// NOTE: This is for the original Actor/ActorSheet and the variables have since changed.
/** @override */
getData()
{
	// Get the full data from the original class.
	const context = super.getData();

	// context.data is actually the Actor class.
	const actorData = context.data;

	// Save the system for shortening later variable names.
	context.system = actorData.system;

	// If this is a "character" type actor, calculate the derived data.
	if(actorData.type == "character")
		this._prepareCharacterData(actorData);
	
	// Return the updated data.
	return context;
}

// Normally pulled from constants.mjs
export var damageSteps = [[6,3], [3,2], [1,1]]

/**
* Prepare Character type specific data
*/
_prepareCharacterData(actorData)
{
	const systemData = actorData.system;

	// Zero the damage negative.
	systemData.damage_neg = systemData.damage_neg || {};
	systemData.damage_neg.value = 0;

	// Check the stun values.
	for(let i=0; i < damageSteps.length; i++)
	{
		if(systemData.stun.value >= damageSteps[i][0])
		{
			systemData.damage_neg.value += damageSteps[i][1];
			break;
		}
	}

	// Check the physical values.
	for(let i=0; i < damageSteps.length; i++)
	{
		if(systemData.physical.value >= damageSteps[i][0])
		{
			systemData.damage_neg.value += damageSteps[i][1];
			break;
		}
	}
	
	// No return necessary; data altered in-place.
}
```
