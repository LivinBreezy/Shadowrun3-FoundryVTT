The SR3Actor (as its currently named in the JS class, or Character as its known in Foundry) contains all the information required for a player character (PC) in Shadowrun 3. 

As of June 18, 2024, the SR3Actor has the following `system` structure based on the `template.json` file.
## Attributes

Attributes are the main ability scores for an Actor. SR3Actors don't need professional rating, but they're included in the list because they belong to the `attributes` template. Below you will find a list of all attributes and a description of each property of an attribute object.

| **Attribute**       | **ATTRIBUTE_NAME**    |
| ------------------- | --------------------- |
| Body                | `body`                |
| Quickness           | `quickness`           |
| Strength            | `strength`            |
| Intelligence        | `intelligence`        |
| Willpower           | `willpower`           |
| Charisma            | `charisma`            |
| Essence             | `essence`             |
| Reaction            | `reaction`            |
| Magic               | `magic`               |
| Professional Rating | `professional_rating` |
#### *system.attributes.ATTRIBUTE_NAME.base*

The base value of the Actor's given attribute. This is their unaugmented score.

#### *system.attributes.ATTRIBUTE_NAME.value*

The current value of the Actor's given attribute, including manually entered temporary bonuses, active effects, cyberware/bioware, magic, adept powers, etc.

#### *system.attributes.ATTRIBUTE_NAME.min*

The minimum value the Actor's given attribute can be. May not end up being used.

#### *system.attributes.ATTRIBUTE_NAME.temp*

The current manually entered temporary modifier for the Actor's given attribute. This is set by the player on the attribute screen.

#### *system.attributes.ATTRIBUTE_NAME.mod*

A list of items which are currently affecting the Actor's given attribute.