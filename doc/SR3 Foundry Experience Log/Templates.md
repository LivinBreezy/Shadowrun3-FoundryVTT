The `template.json` file tells the game system what properties (fields) each Actor or Item will possess. You can define some templates with properties and assign them to various Actors and Items to save space.

## Actors

Non-obvious things to know about the Actor types will go here.

- Currently nothing.

## Items

Non-obvious things to know about the Item types will go here.

1. I started out with a single skill item and was going to use it everywhere, but I think different skill types need to be more defined so there's currently an active skill, knowledge skill, and language skill instead.
2. Knowledges have "classes" on SR3 p.58, which include Street, Academia, Sixth World, Background, and Interests. Don't get those classes confused with programming terms.
3. I created a Specialization item that I'm hoping can be embedded into any of the Skill items and show up on the skill list like this (for example):
   ![[spec_example.png]]
4. Specialization "type" is listed as "aspect" to match the description on SR3 p.57. It can definitely be changed to something more descriptive.
   