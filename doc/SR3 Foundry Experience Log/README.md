# SR3 Experience Logs

This is my experience log and basic documentation for the FoundryVTT SR3 system. Watch me struggle to figure out the most basics of CSS while being pretty okay at the other stuff.

## Caveats
I am a systems and evolutionary computation person. I am not a web developer and never, ever will be. 

It seems that the majority of the Foundry VTT documentation is incredibly poor and so most of what I know is from trying to go through the [Boilerplate tutorial/code repository](https://github.com/asacolips-projects/boilerplate) (thanks Matt). I've read that the community Discord servers have information on how to do things if you search them, but I was hoping for more of a manual than a collaboration. 

A good amount of the development process behind Foundry may be based on a bunch of webdev knowledge I simply do not have, so maybe this is actually far more intuitive for a more knowledgeable person. For me, it is sometimes torture.