The system needs some features and I can add them here as a pseudo-roadmap. No guarantees on order, despite the fact that I'll inherently have an order to them.

## Starting State

The sheet has started out with a simple stun/physical counter for the condition monitor (no overflow), a box for initiative dice, a box for reaction (which does not calculate automatically), a box for damage negatives (not auto), and a box for any other initiative changes.

## Phase 1

The first wave should include the following changes:

1. Figure out how to auto-calculate stats (***done***).
2. Add all attributes to the character sheet and have them defined in `template.json`.
	a. This includes automatically calculating Reaction from said attributes.
	b. This should include Magic, but only show it if the character is Awakened.
3. Make Attributes rollable by clicking the name, which links them to the current rolling system.
4. Add a condition monitor for stun and physical and overflow.
5. Add a section for pools. These are visual only and should only show ones the character has.

## Phase 2

This wave is a bit simpler, but has a few crucial abstractions.

1. Modify or create chat roll cards to handle more passed information. Right now it's pretty simple:
   ![[img/old_roll.png]] ![[img/old_open.png]]
	a. We should change the first line to describe the roll (attribute, skill, etc.). 
	b. If a roll is defaulted, it should be able to include information like the skill, what it defaults to, what the TN modifier is, etc., but that functionality is in Phase 3 and not necessary now.
	c. We should change the colours and/or text to show when a glitch occurs.
	d. Add a homebrew feature to the system's options that lets you switch between SR3 and SR5 glitches, then have the cards adapt to it.
1. Make a roll window that confirms a few things before rolling:
	a. The current TN.
	b. If the user wants to use any pool.
	c. Dice pool modifiers (for temporary Karma reroll functionality).

## Phase 3

The third wave is where we'll figure out how Item collections work. Right now I don't know how lists of items are attached to character sheets, but hopefully we'll know when this is done. 

1. Define the Skill item, which includes:
	a. Define the Skill properties in `template.json`.
	b. Create a `SkillItem` class for handling the properties in-game.
	c. Create a Skill item sheet with HTML and CSS. Maybe like this:
	![[skill_sheet_mockup.png|400]]	
2. Create a Skill item for every Skill in the game (see Skill List PDF). Start with Active skills only.
3. Add a Skills "inventory" to the character sheet so it can hold any number of Skill items.
4. Make Skills rollable, which links them to the current rolling system.

## Phase 4

TBA