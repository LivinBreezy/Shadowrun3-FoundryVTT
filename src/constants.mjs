
/******************************************
 *	 MODIFY THESE CONSTANTS TO SUIT YOUR
 *   TEST ENVIRONMENT. MAKE SURE YOUR GIT
 *   IGNORE FILE IS UP TO DATE!
 ******************************************/
export var systemFolder = "shadowrun3dev";

export var damageSteps = [[6,3], [3,2], [1,1]];
export var damageTypes = ["Serious", "Moderate", "Light"];