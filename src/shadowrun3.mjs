// Import the system directory name.
import { systemFolder } from "./constants.mjs";

// Import the Actor and ActorSheet for SR3.
import { SR3Actor } from "./sheets/scripts/sr3Actor.mjs";
import { SR3ActorSheet } from "./sheets/scripts/sr3ActorSheet.mjs";

// Impoort the Item and ItemSheet for SR3.
// TODO: Set up items.

// Import Custom Handlebar Templates.
import { preloadHandlebarsTemplates } from "./sheets/scripts/templates.mjs"

/*
 *  Main system initialization function.
 */
Hooks.once("init", function()
{
    // Create a global space system object for easy access.
    game.shadowrun3dev = {
        SR3Actor,
    };

    // Set the base initiative calculation for the system.
    CONFIG.Combat.initiative = {
        formula: "(@initiative.regular.dice.value)d6 + @initiative.regular.base.value",
        decimals: 0
    };

    // Define the custom document classes for Actors and Items.
    CONFIG.Actor.documentClass = SR3Actor;
    //CONFIG.Item.documentClass = SR3Item;

    // TODO: Figure out what this actually means.
    // Active Effects are never copied to the Actor,
    // but will still apply to the Actor from within the Item
    // if the transfer property on the Active Effect is true.
    CONFIG.ActiveEffect.legacyTransferral = false;

    console.log("Shadowrun 3 | Registering the Shadowrun Actor Sheets.");

    // Unregister the default/core ActorSheet.
    Actors.unregisterSheet("core", ActorSheet);

    // Register the SR3ActorSheet as the new default sheet.
    Actors.registerSheet(systemFolder, SR3ActorSheet, {
        makeDefault: true,
    });

    // Preload Handlebars templates.
    return preloadHandlebarsTemplates();
});