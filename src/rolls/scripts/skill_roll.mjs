import
{
    systemFolder
}
from "../../constants.mjs"

const diceSizes = [6, 12, 24, 30, 100];

export class SR3SkillRoll
{
	static async init(controls, html)
    {
		// Define the HTML for the scene control button which opens the dice rolling panel.
		// It must be defined literal with $ or it will disappear after the function is over.
        const buttonHTML = $(
            `
            <li class="scene-control sdr-scene-control" data-control="simple-dice-roller" title="Simple Dice Roller">
                <i><div class="skillButtonDiv"></div></i>
            </li>
            `
        );
		
		// Define the HTML for the dice rolling panel.
        const panelHTML = `
            <ol class="sub-controls app control-tools sdr-sub-controls">
                <li id="SDRpopup" class="simple-dice-roller-popup control-tool">
                </li>
            </ol>
        `;

		
		// Add the button HTML to the main controls menu class.
        html.find(".main-controls").append(buttonHTML);
		
		// Add the dice rolling panel HTML to the main HTML code.
		// We will add more controls/classes to it later.
        html.append(panelHTML);

		// Add an event listener to the dice rolling button to make the dice rolling panel
		// appear when it is clicked. The PopupPanel method is a member of this class.
        buttonHTML[0].addEventListener('click', ev => this.PopupPanel(ev, html));

		// Finally, proceed to populating the content of the dice rolling panel.
        this.fillPanel(html);
    }
	
	static async fillPanel(html)
    {
		// Generate the HTML for handling the visual aspects of the dice rolling panel.
        const panelContents = $(this.generatePanel(15));

		// Remove old dice panel components so we can add new ones.
        html.find('.simple-dice-roller-popup ul').remove();
        html.find('.simple-dice-roller-popup textarea').remove();
        html.find('.simple-dice-roller-popup div').remove();

		// Add our new component HTML to the dice roller.
        html.find('.simple-dice-roller-popup').append(panelContents);

		// Create on-click events for the rows of numerical/Open buttons.
		// These buttons will select number of dice and target numbers.
        html.find('.num_dice_table li').click(ev => this.populateNumDice(ev, html));
        html.find('.target_num_table li').click(ev => this.populateTargetNum(ev, html));
		
		// Create an on-click event for the dice rolling button. This will roll
		// the requested dice and print the results in the chat window.
        html.find('.dice-calculator__roll').click(ev => this.rollDice(ev, html));
    }

	static generatePanel(maxDiceCount, enableFateDice)
    {
		// Create an empty typeless list to hold HTML code.
        let s = [];

		// Add a class that holds the title for the number of dice.
        s.push('<div class="num_dice_title">Select the Number of Dice to Roll')
        s.push('</div>');

		// Add a class that creates a row of numbers in the form of a table.
		// These numbers will be clickable in the future as buttons for selecting
		// the number of dice a user would like to roll.
        s.push('<div class="num_dice_table">');
        s.push(this.createNumberRow(6, maxDiceCount, false));
        s.push('</div>');

		// Add a text area for displaying/inputting the current number of dice.
		// The user can add a custom value here if they wish.
        s.push('<textarea type="text" placeholder="Number of Dice" id="num_dice_field" class="dr-num-dice-input">');
        s.push('</textarea>');

		// Add a class that holds the title for entering the target number..
        s.push('<div class="target_num_title">Enter the target number. For an Open Roll, click the Open button.')
        s.push('</div>');

		// Add a class that creates a row of numbers in the form of a table.
		// These numbers will be clickable in the future as buttons for selecting
		// the desired target number for the user's roll.
        s.push('<div class="target_num_table">')
        s.push(this.createNumberRow(6, maxDiceCount, true));
        s.push('</div>');

		// Add a text area for displaying/inputting the current target number.
		// The user can add a custom value here if they wish.
        s.push('<textarea type="text" placeholder="Target Number" id="target_num_field" class="tn-input">');
        s.push('</textarea>');

        // Add a button for initiating the dice roll.
        s.push('<div class="dice-calculator__roll sr3-roll-button" id="roll-button">Make Skill Roll</div>');
		
		// Join the HTML list into a string of HTML code and return it.
        return s.join('');
    }
	
	static createNumberRow(diceValue, maxRowValue, hasOpenRoll)
    {
		// Create an empty typeless list to hold HTML code.
        let s = [];

		// Add the beginning of an unordered list.
        s.push('<ul>');

		// We use a for loop to create a row of numbers in the range [1, maxRowValue].
        for(let i = 1; i <= maxRowValue; ++i)
        {
			// Determine if this cell should be labeled "Open".
			let isOpen = i == 1 && hasOpenRoll;
			
			// Create a list item for the current cell. If it is Open, it will 
			// be labeled Open, otherwise it is labeled with an integer value.
			s.push('<li data-dice-type="', diceValue, '" data-dice-roll="', (isOpen ? 'open' : i), '"');
		
			// Add the visible number/Open value to the list item and close it.
			s.push('>' + (isOpen ? 'Open' : i) + '</li>');
        }		

		// Add the end of the unordered list.
        s.push('</ul>');

		// Join the HTML list into a string of HTML code and return it.
        return s.join('');
    }
	
    static async rollDice(event, html)
    {
        // Stop the default on-click event from occurring.
        event.preventDefault();

        // Get the number of dice input box from the template.
        let numDiceInput = document.getElementById('num_dice_field').value;

        // Get the target number box from the template.
        let targetNumInput = document.getElementById('target_num_field').value;

        // Close the open roll input window.
        this.close(event, html);

        // Stop processing if an input was empty.
        if(numDiceInput.length == 0 || numDiceInput < 1)
            return;

		// Convert the numDiceInput string to a number.
        let numDice = Number(numDiceInput);
		
		// Process the open dice roll.
        let diceVals = await this.processRolls(event, html, numDice);
		
		//console.log(diceVals);
			
        // We're going to do an open roll if the target number window is blank, or 1.
        if(targetNumInput.length == 0 || targetNumInput <= 1 || isNaN(targetNumInput))
        {            
		    // Find the largest value in the open roll results.
            let {rolls, largest} = this.determineLargest(diceVals);

            // Print the roll results to the chat window.
            this.sendOpenResultChatMessage(event, html, rolls, largest);			
        }

        // Call the skill roll functions.
        else
        {
          // Convert the targetNumInput string to a number.
          let targetNumber = Number(targetNumInput);

          // Calculate success for each of the dice rolls.
          let {rolls, successes} = this.determineSuccess(diceVals, targetNumber);

          // Print the roll results to the chat window.
          this.sendSkillResultChatMessage(event, html, rolls, successes, targetNumber);
        }
    }

    static determineLargest(diceVals)
    {
        // Create an empty data array.
        let dataArray = [];

        // We haven't found the largest value yet.
        let found = false;

        // Store the largest value. Defaults to -1.
        let largest = -1;

        // Store the index of the largest value. Defaults to 0.
        let largestIndex = 0;

        // Check every die in the value list.
        for(let i = 0; i < diceVals.length; i++)
        {
            // Compare the current die value to the largest so far.
            if(diceVals[i] > largest)
            {
                // If the die value is larger, it becomes the largest.
                largest = diceVals[i];

                // Store the new index of the current largest.
                largestIndex = i;
            }

            // Create a simple dice object with the value and false flag.
            let diceData =
            {
                diceVal: String(diceVals[i]),
                isLargest: false
            };

            // Add this new dice object to the data array.
            dataArray.push(diceData);
        }

        // Set the correct largest dice object's flag to true.
        dataArray[largestIndex].isLargest = true;

        // Return the data array and the largest value.
        return { rolls: dataArray, largest: largest };
    }

    static determineSuccess(diceVals, targetNumber)
    {
        // Create an empty data array.
        let dataArray = [];

        // Create a variable for counting successes.
        let successes = 0;

        // Create a variable for holding success.
        let success = false;

        // Check every die in the value list.
        for (let i = 0; i < diceVals.length; i++)
        {
            // Set the success state to false.
            success = false;

            // If we beat the target number, we're successful. Count it.
            if (diceVals[i] >= targetNumber)
            {
                success = true;
                successes++;
            }

            // Create a simple dice object with the value and success flag.
            let diceData = {
                diceVal: String(diceVals[i]),
                isSuccess: success
            };

            // Add this new dice object to the data array.
            dataArray.push(diceData);
        }

        // Return the data array and the largest value.
        return {
            rolls: dataArray,
            successes: successes
        };
    }
	
	static async processRolls(event, html, numDice)
    {
        // Create an empty array for adjusted dice rolls.
        let finalRolls = [];
		
		// Create an empty array for holding the rolls for Dice So Nice!
		let finalRollObjects = [];

        // Create a variable to hold die results.
        let result = 0;

        // Create a variable for holding a total roll.
        let total = 0;

        // Roll a number of dice defined by numDice.
        for (let i = 0; i < numDice; i++)
        {
            // Make sure the total is 0 before the exploding dice start.
            total = 0;

            do {
                // Create a new 1d6 roll object and async evaluate it.
                let roll = new Roll("1d6");	
                await roll.evaluate();
				
				// Save the Roll object.
				finalRollObjects.push(roll);
				
                // Convert the result to an int and store it.
                result = roll.total;

                // Add the result to the total.
                total = total + result;

                // If the last result was 6, roll another die.
            } while (result == 6);

            // Add the adjusted die value to the rolls array.
            finalRolls.push(total);
        }

		// If Dice So Nice! is installed, roll the dice here.
		if(game.dice3d)
		{
			// Create a data object for Dice So Nice! structure.
			let data = {throws:[{dice:[]}]};

			// Loop through the final rolls and add appropriate dice to the object.			
			for(let i = 0; i < finalRolls.length; i++)
			{
				// Add a new dice object entry.
				data.throws[0].dice.push({});
											
				// Calculate the sides of the die. Round up to nearest multiple of 6.
				let sides = 6;
				
				// Get the next appropriate die size to hold the value.
				for(let j = 0; j < diceSizes.length-1; j++)
					if(finalRolls[i] > diceSizes[j])
						sides = diceSizes[j+1]
					else
						break
							
				// Fill in the object.
				data.throws[0].dice[i].result = finalRolls[i];
				data.throws[0].dice[i].resultLabel = finalRolls[i];
				data.throws[0].dice[i].type = "d" + sides;
				data.throws[0].dice[i].vectors = [];
				data.throws[0].dice[i].options = {};
			}
			
			//console.log(data);
			
			// Show the 3D dice.
			game.dice3d.show(data);		
		}

        // Return the adjusted rolls array.
        return finalRolls;
    }
	
    static async sendOpenResultChatMessage(event, html, rolls, largest)
    {
        // Store the template path for the open roll chat results.
        const template = "systems/" + systemFolder + "/src/rolls/templates/sr3-open-roll-message.html";

        // Generate template data with the correct data fields.
        const templateData =
        {
            numDice: String(rolls.length),
            greatest: String(largest),
            diceData: rolls
        };

        // Wait for it to render the chat message template.
        const htmlForm = await renderTemplate(template, templateData);

        // Create the results chat message.
        ChatMessage.create({
            user: game.user.id,
            speaker:
            {
                actor: game.user.character
            },
            content: htmlForm
        });
    }

    static async sendSkillResultChatMessage(event, html, rolls, successes, targetNumber)
    {
        // Store the template path for the open roll chat results.
        const template = "systems/" + systemFolder + "/src/rolls/templates/sr3-skill-roll-message.html";

        // Generate template data with the correct data fields.
        const templateData = {
            numDice: String(rolls.length),
            tN: String(targetNumber),
            diceData: rolls,
            numSuccesses: successes
        };

        // Wait for it to render the chat message template.
        const htmlForm = await renderTemplate(template, templateData);

        // Create the results chat message.
        ChatMessage.create(
        {
            user: game.user.id,
            speaker:
            {
                actor: game.user.character
            },
            content: htmlForm
        });
    }

	static async PopupPanel(event, html)
    {
        if (html.find('.sdr-scene-control').hasClass('active'))
        {
            this.close(event, html);
        }
        else
        {
            this.open(event, html);
        }
    }

    static async close(event, html)
    {
        html.find('.sdr-scene-control').removeClass('active');
        html.find('.sdr-sub-controls').removeClass('active');
        html.find('.scene-control').first().addClass('active');

        event.stopPropagation();
    }

    static async open(event, html)
    {
		this.generatePanel(html);
        html.find('.scene-control').removeClass('active');
        html.find('.sub-controls').removeClass('active');
        html.find('.sdr-scene-control').addClass('active');
        html.find('.sdr-sub-controls').addClass('active');
        event.stopPropagation();
    }
	
	static async populateNumDice(event, html)
    {
        var numberOfDice = event.target.dataset.diceRoll;
        document.getElementById('num_dice_field').value = numberOfDice;
    }

    static async populateTargetNum(event, html)
    {
        var targetNumber = event.target.dataset.diceRoll;
        document.getElementById('target_num_field').value = targetNumber;
		
		let isMake = targetNumber <= 1 || isNaN(targetNumber);
        
		document.getElementById('roll-button').innerHTML = (isMake ? "Make Open Roll" : "Make Skill Roll");
    }
}
