
/**
 * Import the system folder path and roll objects.
 *
 */
import { systemFolder } from "../../constants.mjs"
import { SR3SkillRoll } from "./skill_roll.mjs";

/**
 * When the scene initializes, run this set of functions.
 *
 */
Hooks.once("init", () =>
{
	// Register the settings for the SR3 Dice Rollers.
	game.settings.register("sr3-dice-roller", "maxDiceCount",
	{
		name: game.i18n.localize("SR3SkillRoll.maxDiceCount.name"),
		hint: game.i18n.localize("SR3SkillRoll.maxDiceCount.hint"),
		scope: "world",
		config: true,
		default: 8,
		type: Number
	});

	// Confirm that the rolling system was loaded successfully.
	console.log("Shadowrun 3 | Dice Roller Loaded Successfully");
});

/**
 * When the scene controls are created, make sure to initialize
 * the open roll and skill roll buttons.
 *
 */
Hooks.on("renderSceneControls", (controls, html) =>
{
	SR3SkillRoll.init(controls, html);
});

/**
 * If there are any hooks necessary when the scene is ready, add
 * them all here. Empty by default.
 */
Hooks.on("ready", () =>
{
	// Add any ready hooks here.
});
