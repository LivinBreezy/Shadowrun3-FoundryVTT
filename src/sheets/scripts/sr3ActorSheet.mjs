import { systemFolder, damageSteps, damageTypes } from "../../constants.mjs";

export class SR3ActorSheet extends ActorSheet 
{    
    /** @override */
    static get defaultOptions() 
    {        
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: [systemFolder, "sheet", "actor"],
            template: "systems/" + systemFolder + "/src/sheets/templates/actors/actor-character-sheet.html",
            width: 900,
            height: 700,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sr3-sheet-body", initial: "init" }]
        });
    }

    /** @override */
    get template() 
    {
        return "systems/" + systemFolder + "/src/sheets/templates/actors/actor-character-sheet.html";
    }

    /** @override */
    getData() 
    {
        console.debug("SHADOWRUN 3 | Entering data retrieve.");

        // Retrieve the data structure from the base sheet. You can inspect or log
        // the context variable to see the structure, but some key properties for
        // sheets are the actor object, the data object, whether or not it's
        // editable, the items array, and the effects array.
        const context = super.getData();

        console.debug("SHADOWRUN 3 | getData Context is:");
        console.debug(context);

        // Use a safe clone of the actor data for further operations.
        const actorData = context.data;

        console.debug("SHADOWRUN 3 | Assigning data. actorData is:");
        console.debug(actorData);

        // Add the actor's data to context.data for easier access, as well as flags.
        context.system = actorData.system;
        context.flags = actorData.flags;

        console.debug("SHADOWRUN 3 | Assign context.");

        // Prepare character derived data.
        if(actorData.type == "character")
        {
            console.debug("SHADOWRUN 3 | Prepping data.");
            this._prepareCharacterData(context);
            this._prepareCharacterItems(context);
        }

        console.debug("SHADOWRUN 3 | Prep complete. Returning context:");
        console.debug(context);

        return context;
    }

    /**
     * Activate event listeners using the prepared sheet HTML
     * @param html {jQuery}   The prepared HTML object ready to be rendered into the DOM
     */
    activateListeners(html) 
    {
        super.activateListeners(html);

        html.find(".rollable[data-action]").click(this._onSheetAction.bind(this));
    }
    
    /**
     * Handle mouse click events for character sheet actions
     * @param {MouseEvent} event    The originating click event
     * @private
     */
    _onSheetAction(event) 
    {
        event.preventDefault();
        const button = event.currentTarget;
        switch( button.dataset.action ) 
        {
            case "rollInitiative":
                return this.actor.rollInitiative({createCombatants: true});
        }
    }

    /**
    * Prepare Character type specific data
    */
    _prepareCharacterData(context) 
    {
        // Make modifications to data here. For example:
        const systemData = context.system;

        // Reset the character's injury modifier and statuses.
        systemData.track.injury_modifier = 0;
        systemData.track.stun.status = "Healthy";
        systemData.track.physical.status = "Healthy";

        // TODO: Incorporate Pain Tolerance (High and Low).

        // Check the stun values.
        for(let i=0; i < damageSteps.length; i++)
        {
            if(systemData.track.stun.value >= damageSteps[i][0])
            {
                systemData.track.injury_modifier += damageSteps[i][1];
                systemData.track.stun.status = damageTypes[i];
                break;
            }
        }

        // Check the physical values.
        for(let i=0; i < damageSteps.length; i++)
        {
            if(systemData.track.physical.value >= damageSteps[i][0])
            {
                systemData.track.injury_modifier += damageSteps[i][1];
                systemData.track.physical.status = damageTypes[i];
                break;
            }
        }

        // TODO: Set Physical Overflow.
    }

    /**
    * Prepare Item type specific data
    */
    _prepareCharacterItems(context) 
    {
        console.debug("SHADOWRUN 3 | Preparing Item Data");
        console.debug(context);

        // Store the properties locally for easier access.
        const actorData = context.actor;
        const systemData = context.system;

        // Get all the actor's items categorized by type.
        const itemsByCategory = context.actor.itemTypes;
        
        console.debug("SHADOWRUN 3 | All item type lists are:");
        console.debug(itemsByCategory);

        // Now that we have them in separate lists, we can add them to the context object.
        context.active_skills = itemsByCategory.active_skill;
        context.knowledge_skills = itemsByCategory.knowledge_skill;
        context.language_skills = itemsByCategory.language_skill;

        console.debug("SHADOWRUN 3 | Finished prep. Context is this before return:");
        console.debug(context);
    }
}
