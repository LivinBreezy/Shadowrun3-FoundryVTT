import { systemFolder } from "../../constants.mjs";

export const preloadHandlebarsTemplates = async function() {
  return loadTemplates([

    // Actor partials.
    "systems/" + systemFolder + "/src/sheets/templates/actors/actor-bodies/bio-body.html",
    "systems/" + systemFolder + "/src/sheets/templates/actors/actor-bodies/init-body.html",
    "systems/" + systemFolder + "/src/sheets/templates/actors/actor-bodies/placeholder.html",
  ]);
};

