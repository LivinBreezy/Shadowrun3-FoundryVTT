

export class SR3Combat extends Combat
{
	async rollInitiative(ids, options)
	{
		const combat = await super.rollInitiative(ids, options) as SR3Combat;
		
		if(this.initiativePass = 1)
			await combat.update({turn: 0});
		
		return combat;
	}
	
	async rollAll()
	{
		const combat = await super.rollAll() as SR5Combat;
		
		combat.update({turn: 0});
		
		return combat;
	}
}